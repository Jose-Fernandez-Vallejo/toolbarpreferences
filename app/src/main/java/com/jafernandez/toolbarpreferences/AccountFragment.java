package com.jafernandez.toolbarpreferences;

import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;

public class AccountFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.account_preferences);
        initPreferenceUser();
        initPreferencePassword();
    }

    private void initPreferencePassword() {
        EditTextPreference edpPass = getPreferenceManager().findPreference(getResources().getString(R.string.key_password));
        edpPass.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
            @Override
            public void onBindEditText(@NonNull EditText editText) {

                editText.setSingleLine();
                editText.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                //esto debe ir lo ultimo o no lo hara correctamente
                editText.selectAll();
            }
        });
    }

    /**
     * Este metodo inicializa la preferencia EdittextPreference User
     */
    private void initPreferenceUser() {
        EditTextPreference edpUser = getPreferenceManager().findPreference(getResources().getString(R.string.key_user));
        edpUser.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
            @Override
            public void onBindEditText(@NonNull EditText editText) {

                editText.setSingleLine();
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                //esto debe ir lo ultimo o no lo hara correctamente
                editText.selectAll();
            }
        });
    }
}
