package com.jafernandez.toolbarpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
    addPreferencesFromResource(R.xml.setting_preferences);

    //se quierere coger el evento cuando se modifique el tono
        onSharedPreferenceChanged(PreferenceManager.getDefaultSharedPreferences(getActivity()),getString(R.string.key_ringtone_notification));
    }

    @Override
    public void onResume() {
        super.onResume();
        //se debe registrar el listener
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);

        if (key.equals(getString(R.string.key_ringtone_notification)))
        {
            ListPreference listPreference = (ListPreference)preference;
            int prefIndex = listPreference.findIndexOfValue(sharedPreferences.getString(key,""));
            if (prefIndex >= 0)
            {
                preference.setSummary(listPreference.getEntries()[prefIndex]);
            }
            else
            {
                preference.setSummary(sharedPreferences.getString(key,""));
            }
        }
    }
}
